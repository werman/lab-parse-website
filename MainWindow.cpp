#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QWebPage>
#include <QWebFrame>
#include <QWebSettings>
#include <QWebElement>
#include <QUrlQuery>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include "MainWindow.h"
#include "ui_MainWindow.h"

const QMap<QString, QString> MainWindow::k_olxCities =
    QMap<QString, QString>({{"kha", "Kharkiv"}, {"ko", "Kiev"}});
const QString MainWindow::k_olxRequestTemplate =
    "http://olx.ua/%1/q-%2/?search";

const QMap<int, QString> MainWindow::k_railroadCities =
    QMap<int, QString>({{2204001, "Kharkiv"},
                        {2200001, "Kiev"},
                        {2218000, "Lviv"},
                        {2210720, "Dnepr"}});
const QString MainWindow::k_railroadRequestTemplate =
    "http://www.pz.gov.ua/rezervGR/aj_g60.php";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), m_ui(new Ui::MainWindow),
      m_olxNetworkManager(new QNetworkAccessManager(this)),
      m_railroadNetworkManager(new QNetworkAccessManager(this)) {
  m_ui->setupUi(this);

  connect(m_olxNetworkManager, SIGNAL(finished(QNetworkReply *)), this,
          SLOT(olxUrlLoaded(QNetworkReply *)));

  connect(m_railroadNetworkManager, SIGNAL(finished(QNetworkReply *)), this,
          SLOT(railroadUrlLoaded(QNetworkReply *)));

  addOlxCities();
  addRailroadCities();
}

MainWindow::~MainWindow() {
  delete m_ui;
  delete m_olxNetworkManager;
  delete m_railroadNetworkManager;
}

void MainWindow::on_btnGet_clicked() {
  const auto &&productName = m_ui->editProductName->text();
  const auto &&selectedData = m_ui->chooseCity->currentData();
  auto selectedCity = selectedData.toString();

  if (productName.size() != 0 && selectedCity.size() != 0) {
    fireRequestToOlx(productName, selectedCity);
  } else {
  }
}

void MainWindow::olxUrlLoaded(QNetworkReply *reply) {
  QVector<OlxOffer> offers;

  QByteArray content = reply->readAll();

  QWebPage webPage;
  auto settings = webPage.settings();
  settings->setAttribute(QWebSettings::JavascriptEnabled, false);

  QWebFrame *frame = webPage.mainFrame();
  frame->setHtml(content);

  auto offersEl = frame->findAllElements("#offers_table > tbody > tr");

  for (const auto &offerEl : offersEl) {
    OlxOffer offer;

    auto linkEl = offerEl.findFirst("a.linkWithHash.detailsLink");
    if (!linkEl.isNull()) {
      offer.link = linkEl.attribute("href");
    }

    auto nameEl = offerEl.findFirst("a.linkWithHash.detailsLink > strong");
    if (!nameEl.isNull()) {
      offer.name = nameEl.toInnerXml();
    }

    auto priceEl = offerEl.findFirst(".price > strong");
    if (!priceEl.isNull()) {
      offer.price = priceEl.toInnerXml();
    }

    if (offer.name.size() != 0 && offer.price.size() != 0) {
      offers.push_back(offer);
    }
  }

  m_ui->tableResult->setRowCount(offers.size());
  int idx = 0;
  for (const auto &offer : offers) {
    m_ui->tableResult->setItem(idx, 0, new QTableWidgetItem(offer.name));
    m_ui->tableResult->setItem(idx, 1, new QTableWidgetItem(offer.price));
    m_ui->tableResult->setItem(idx, 2, new QTableWidgetItem(offer.link));
    idx++;
  }
}

void MainWindow::addOlxCities() {
  for (const auto &city : k_olxCities.toStdMap()) {
    m_ui->chooseCity->addItem(city.second, QVariant(city.first));
  }
}

void MainWindow::fireRequestToOlx(const QString &productName,
                                  const QString &selectedCity) {
  QString escapedProductName = QUrl::toPercentEncoding(productName);
  QString url = k_olxRequestTemplate.arg(selectedCity, escapedProductName);
  m_olxNetworkManager->get(QNetworkRequest(QUrl(url)));
}

void MainWindow::on_btnGetSchedule_clicked() {
  const auto &&selectedDataCityFrom = m_ui->chooseFrom->currentData();
  const auto &&selectedDataCityTo = m_ui->chooseTo->currentData();
  const auto &&date = m_ui->selectDate->date();

  if (selectedDataCityFrom.isNull() || selectedDataCityTo.isNull()) {
    return;
  }

  int cityFrom = selectedDataCityFrom.toInt();
  int cityTo = selectedDataCityTo.toInt();

  fireRequestToRailroad(cityFrom, cityTo, date);
}

void MainWindow::addRailroadCities() {
  for (const auto &city : k_railroadCities.toStdMap()) {
    m_ui->chooseFrom->addItem(city.second, QVariant(city.first));
    m_ui->chooseTo->addItem(city.second, QVariant(city.first));
  }
}

void MainWindow::fireRequestToRailroad(int cityFrom, int cityTo,
                                       const QDate &date) {
  QUrlQuery postData;
  postData.addQueryItem("kstotpr", QString::number(cityFrom));
  postData.addQueryItem("kstprib", QString::number(cityTo));
  postData.addQueryItem("sdate", date.toString("dd-MM-yyyy"));

  QNetworkRequest request(k_railroadRequestTemplate);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    "application/x-www-form-urlencoded");
  m_railroadNetworkManager->post(
      request, postData.toString(QUrl::FullyEncoded).toUtf8());
}

void MainWindow::railroadUrlLoaded(QNetworkReply *reply) {
  QVector<Train> trains;

  QByteArray content = reply->readAll();

  QJsonParseError err;
  QJsonDocument doc = QJsonDocument::fromJson(content, &err);

  if (doc.isObject()) {
    QJsonObject obj = doc.object();

    auto jTrainsArray = obj.value("trains").toArray();
    for (auto jTrainInfo : jTrainsArray) {
      Train train;

      auto jInfo = jTrainInfo.toObject();
      train.start = jInfo.value("otpr").toString();
      train.luxury = jInfo.value("l").toInt();
      train.coupe = jInfo.value("k").toInt();
      train.thirdClass = jInfo.value("p").toInt();
      train.sitting = jInfo.value("o").toInt();

      trains.push_back(train);
    }
  }

  m_ui->tableTrains->setRowCount(trains.size());
  int idx = 0;
  for (const auto &train : trains) {
    m_ui->tableTrains->setItem(idx, 0, new QTableWidgetItem(train.start));
    m_ui->tableTrains->setItem(
        idx, 1, new QTableWidgetItem(QString::number(train.sitting)));
    m_ui->tableTrains->setItem(
        idx, 2, new QTableWidgetItem(QString::number(train.thirdClass)));
    m_ui->tableTrains->setItem(
        idx, 3, new QTableWidgetItem(QString::number(train.coupe)));
    m_ui->tableTrains->setItem(
        idx, 4, new QTableWidgetItem(QString::number(train.luxury)));
    idx++;
  }
}
