#-------------------------------------------------
#
# Project created by QtCreator 2015-12-12T23:47:32
#
#-------------------------------------------------

QT       += core gui network webkit webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab-parse-website
TEMPLATE = app

CONFIG   += c++11

SOURCES += main.cpp\
        MainWindow.cpp

HEADERS  += MainWindow.h

FORMS    += MainWindow.ui

CONFIG(debug, debug|release) {
    DESTDIR = build/release
}
CONFIG(release, debug|release) {
    DESTDIR = build/debug
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.u
