#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QNetworkAccessManager;
class QNetworkReply;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void on_btnGet_clicked();
    void on_btnGetSchedule_clicked();

    void olxUrlLoaded(QNetworkReply* reply);
    void railroadUrlLoaded(QNetworkReply* reply);
private:
    static const QMap<QString, QString> k_olxCities;
    static const QString k_olxRequestTemplate;

    struct OlxOffer {
        QString name;
        QString price;
        QString link;
    };

    static const QMap<int, QString> k_railroadCities;
    static const QString k_railroadRequestTemplate;

    struct Train {
        QString start;
        int luxury;
        int sitting;
        int thirdClass;
        int coupe;
    };

    Ui::MainWindow *m_ui;
    QNetworkAccessManager *m_olxNetworkManager;
    QNetworkAccessManager *m_railroadNetworkManager;

    void addOlxCities();

    void fireRequestToOlx(const QString &productName, const QString &selectedCity);

    void addRailroadCities();
    void fireRequestToRailroad(int cityFrom, int cityTo, const QDate& date);
};

#endif // MAINWINDOW_H
